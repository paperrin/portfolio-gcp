const express = require('express');
const bodyParser = require('body-parser');
const { body, validationResult } = require('express-validator');
const nodeMailer = require('nodemailer');
const { google } = require('googleapis');
const secretsReader = require('./secretsReader');
const helmet = require('helmet');

const app = express();
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: false }));

function createSmtpTransport({ id, mail, secret, refreshToken }) {
	const oAuth2Client = new google.auth.OAuth2(
		id,
		secret,
		'https://developers.google.com/oauthplayground'
	);
	oAuth2Client.setCredentials({
		refresh_token: refreshToken,
	});
	const accessToken = oAuth2Client.getAccessToken();

	return nodeMailer.createTransport({
		service: 'gmail',
		auth: {
			type: 'OAuth2',
			user: mail,
			clientId: id,
			clientSecret: secret,
			refreshToken: refreshToken,
			accessToken: accessToken,
		},
	});
}

function validate() {
	return [
		body('name').trim().isString().notEmpty().escape(),
		body('mail').trim().isEmail().escape(),
		body('text').trim().isString().notEmpty().escape(),
	];
}

app.post('/', validate(), async (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({
			errors: errors.array(),
		});
	}

	const authInfo = await secretsReader.getSecretValue('client_auth_info');
	const form = { name, mail, text } = req.body;

	const mailOptions = {
		from: authInfo.mail,
		to: authInfo.mail,
		subject: 'Portfolio Contact Form Submission',
		generateTextFromHTML: true,
		html: `
				<b>Name:</b><br />
				${form.name}<br />
				<br />
				<b>Mail:</b><br />
				${form.mail}<br />
				<br />
				<b>Message:</b><br />
				${form.text}<br />
			`,
	};

	const smtpTransport = createSmtpTransport(authInfo);
	smtpTransport.sendMail(mailOptions)
		.then(() => {
			res.status(200).send({
				data: {
					code: 200,
					message: "Mail sent",
				}
			})
		})
		.catch(error => {
			res.status(500).send({
				error: {
					code: 500,
					message: error.toString(),
				}
			})
		})
		.finally(() => {
			smtpTransport.close();
		});
});

exports.submitContactForm = app;