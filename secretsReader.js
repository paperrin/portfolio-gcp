const { SecretManagerServiceClient } = require('@google-cloud/secret-manager');

class SecretReader {
	constructor() {
		this.SecretManager = new SecretManagerServiceClient();
	}

	async getSecretValue(secret, version = 'latest') {
		const name = `projects/${process.env.project}/secrets/${secret}/versions/${version}`;
		const [ vs ] = await this.SecretManager.accessSecretVersion({ name });

		return JSON.parse(vs.payload.data.toString('utf8'));
	}

	async getAllSecretValues(secrets) {
		const allCalls = secrets.map(async s => this.getSecretValue(s));

		return await Promise.all(allCalls);
	}
}

module.exports = new SecretReader();